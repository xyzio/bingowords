﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;


namespace LicenceGenerator {
	class LicenceFuncs {

        private string secret = "SAFBUCMDSEKF!#%";
		private string secret2 = "fj*U#%IJifJ:23";
		private string secretWord;

		public LicenceFuncs(string secretWord) {

			this.secretWord = secretWord;
           

		}

		public string generateLicence() {

			return (Encrypt(secret2 + secretWord + secret, this.secretWord));

		}
			
		public bool readLicence(string licence) {
			return (Decrypt(licence));
		}

        private bool Decrypt(string licence)
        {

            string[] splitLicense = licence.Split('-');

            string plainText = (secret2 + splitLicense[0] + secret).ToUpper();

            // Convert plain text into a byte array.
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

            // Because we support multiple hashing algorithms, we must define
            // hash object as a common (abstract) base class. We will specify the
            // actual hashing algorithm class later during object creation.
            HashAlgorithm hash = new SHA1Managed();

            // Compute hash value of our plain text with appended salt.
            byte[] hashBytes = hash.ComputeHash(plainTextBytes);
            char[] returnChar = Convert.ToBase64String(hashBytes).ToCharArray();
            hashBytes = null;

            string returnString = "";
            for (int i = 0; i < returnChar.Length; i++)
            {
                if (returnString.Length >= 8)
                {
                    break;
                }
                if (((returnChar[i] >= 48) && (returnChar[i] <= 57)) || ((returnChar[i] >= 65) && (returnChar[i] <= 90)) || ((returnChar[i] >= 97) && (returnChar[i] <= 122)))
                {
                    returnString += returnChar[i].ToString().ToUpper();
                }
            }

            if (returnString.Length < 8)
            {
                //Pad the rest with 0
                while (returnString.Length < 8)
                {
                    returnString += "0";
                }
            }

            licence = splitLicense[1] + splitLicense[2];

            if (licence == returnString)
            {
                return (true);
            }

            return (false);
        }

		private static string Encrypt(string plainText, string secretWord) {


			// Convert plain text into a byte array.
			byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText.ToUpper());

			// Because we support multiple hashing algorithms, we must define
			// hash object as a common (abstract) base class. We will specify the
			// actual hashing algorithm class later during object creation.
			HashAlgorithm hash = new SHA1Managed();

			// Compute hash value of our plain text with appended salt.
			byte[] hashBytes = hash.ComputeHash(plainTextBytes);
			char[] returnChar = Convert.ToBase64String(hashBytes).ToCharArray();
			hashBytes = null;

			string returnString = "";
			int counter = 0;
			for (int i = 0; i < returnChar.Length; i++) {
				if (returnString.Length > 8) {
					break;
				}
				if (((returnChar[i] >= 48) && (returnChar[i] <= 57)) || ((returnChar[i] >= 65) && (returnChar[i] <= 90)) || ((returnChar[i] >= 97) && (returnChar[i] <= 122))) {
					returnString += returnChar[i].ToString().ToUpper();
					counter = counter + 1;
					if (counter == 4) {
						returnString += "-";
						counter = 0;
                    
					}
				}
			}
			counter = 0;
				if (returnString.Length < 8) {
					//Pad the rest with 0
					while (returnString.Length < 8) {
						returnString += "0";
						counter = counter + 1;
						if (counter == 4) {
							returnString += "-";
							counter = 0;
						}
					}
				}

                if (returnString.EndsWith("-")) {
                    returnString = returnString.Remove(returnString.Length - 1);
                }

            return (secretWord.ToUpper() + "-" + returnString);
		}



	}
}
