﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace LicenceGenerator {
	public partial class LicenceGenerator : Form {
		public LicenceGenerator() {
			InitializeComponent();
		}

		private void LicenceGenerator_Load(object sender, EventArgs e) {

		}

		private void closeButton_Click(object sender, EventArgs e) {
			this.Close();
		}

		private void runButton_Click(object sender, EventArgs e) {
			//Run
			if ((nameTextBox.Text == null) || (nameTextBox.Text == "")) {
				MessageBox.Show("Please enter a valid name!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}
			LicenceFuncs funcs = new LicenceFuncs(nameTextBox.Text);
			textBox2.Text = funcs.generateLicence();
			bool success = funcs.readLicence(textBox2.Text);
			if (success == true) {
				verifyLabel.Show();
			}
			else {
				verifyLabel.Hide();
			}
		}
	}
}
